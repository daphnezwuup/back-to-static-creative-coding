> _Fork_ deze deeltaak en ga aan de slag. 
De instructie vind je in: [docs/INSTRUCTIONS.md](docs/INSTRUCTIONS.md)

# Creative coding
Drie verschillende experimenten. Ik heb drie concepten gekregen van docenten: Page transition on steroïds, Electrical ballenbak bubblegum animation en Scroll flip. 

## Licentie

![GNU GPL V3](https://www.gnu.org/graphics/gplv3-127x51.png)

This work is licensed under [GNU GPLv3](./LICENSE).
